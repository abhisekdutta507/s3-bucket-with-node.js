const express = require('express')
const http = require('http')
const cors = require('cors')
require('dotenv').config()

const app = express()
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const api = require('./config')
app.use('/api', api())

const server = http.Server(app)

/**
 * @description Local server listen.
 */
server.listen(process.env.PORT || 5000, () => {
  console.log(`http://localhost:${process.env.PORT}/ is up`)
})