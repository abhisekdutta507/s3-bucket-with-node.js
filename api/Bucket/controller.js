const fs = require('fs')
const aws = require('aws-sdk')
const { s3 } = require('../../environment/')

class Bucket {

  constructor() {
    aws.config.update({ region: s3.region, accessKeyId: s3.accessKeyId, secretAccessKey: s3.secretAccessKey })
  }

  async find(req, res) {
    return res.status(200).send({ message: 'You are connected to s3 api server.' })
  }

  async transfer(req, res) {
    const target = new aws.S3({ params: { Bucket: s3.bucket.publicImage, ACL: 'public-read' } })
    const body = {
      CopySource: `/${s3.bucket.privateImage}/${req.body.Key}`,
      Key: req.body.Key
    }

    /**
     * @description copy the object from private to public bucket
     */
    const uploaded = await target.copyObject(body).promise()

    return res.status(200).send({ message: 'File transfer completed', data: uploaded })
  }

  async copy(req, res) {
    const target = new aws.S3({ params: { Bucket: s3.bucket[req.body.Bucket], ACL: 'public-read' } })
    const body = {
      CopySource: `/${s3.bucket[req.body.Bucket]}/${req.body.Source}`,
      Key: `${req.body.Target}`
    }

    /**
     * @description copy the object from private to public bucket
     */
    const uploaded = await target.copyObject(body).promise()

    return res.status(200).send({ message: 'File copied successfully', data: uploaded })
  }

  async move(req, res) {
    const target = new aws.S3({ params: { Bucket: s3.bucket[req.body.Bucket], ACL: 'public-read' } })
    const body = {
      CopySource: `/${s3.bucket[req.body.Bucket]}/${req.body.Source}`,
      Key: `${req.body.Target}`
    }

    /**
     * @description copy the object from private to public bucket
     */
    const uploaded = await target.copyObject(body).promise()

    // delete the Source file
    const Objects = [{ Key: req.body.Source }]
    /**
     * @description delete the objects from s3
     */
    target.deleteObjects({ Delete: { Objects } }).promise()

    return res.status(200).send({ message: 'File moved successfully', data: uploaded })
  }

  async download(req, res) {
    /**
     * @description return promise
     */
    const target = fs.createWriteStream(__dirname + `/../../download/${req.body.Key}`)
    const bucket = new aws.S3({ params: { Bucket: s3.bucket[req.body.Bucket] } })
    const file = bucket.getObject({ Key: req.body.Key }).createReadStream()

    file.on('error', (e) => {
      return res.status(404).send({ error: e })
    })

    file.pipe(target).on('error', (e) => {
      return res.status(404).send({ error: e })
    }).on('close', () => {
      return res.status(200).send({ path: target.path })
    })
  }

  async upload(req, res) {
    /**
     * @description create bucket target
     */
    const target = new aws.S3({ params: { Bucket: s3.bucket.publicImage, ACL: 'public-read' } })
    const fileList = req.files.map(({ path, filename, mimetype }) => ({ Key: filename, ContentType: mimetype, Body: fs.createReadStream(`${__dirname}/../../${path}`) }))

    /**
     * @description create promise object
     */
    const promiseList = fileList.map((body) => target.upload(body).promise())
    const uploaded = await Promise.all(promiseList)

    /**
     * @description delete the local files
     */
    req.files.forEach(({ path }) => fs.unlink(path, () => { }))

    /**
     * @description return the response
     */
    res.status(200).send(uploaded)
  }

  async uploadSharp(req, res) {
    res.status(200).send(req.files)
  }

  /**
   * 
   * @param {Object} req.body must have Bucket as string and Keys as an array of strings.
   */
  async delete(req, res) {
    const bucket = new aws.S3({ params: { Bucket: s3.bucket[req.body.Bucket] } })
    const Objects = req.body.Keys.map((Key) => ({ Key }))

    /**
     * @description delete the objects from s3
     */
    const deleted = await bucket.deleteObjects({ Delete: { Objects } }).promise()

    return res.status(200).send({ message: 'Files deleted successfully', data: deleted })
  }

}

const controller = new Bucket()

module.exports = controller