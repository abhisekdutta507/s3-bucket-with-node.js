class File {

  getKey(req, file, cb) {
    cb(null, `media-${file.originalname}`)
  }

}

const helper = new File()

module.exports = helper