This project was created using `npm init`. It is a simple Node JS poject created using Express.

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

## Available Scripts

In the project directory, you can run:
```bash
npm start
```

Runs the app in the development mode.

Open [http://localhost:5000/api/test](http://localhost:5000/api/test) to view it in the browser.

## Learn More

You can learn more in the [Node JS documentation](https://nodejs.org/en/docs/).

To learn Express, check out the [Express 4.x documentation](https://expressjs.com/en/4x/api.html).

**Install Node JS in your machine to start the setup.**

```bash
npm install
```

**Parameters for TRANSFER from private to public object API.**

NOTE: `copies from private to public bucket by default`

API URL: `http://localhost:5000/api/transfer`

METHOD: `PUT`

BODY:
```json
{
  "Key": "bmw-x4.jpg"
}
```

**Parameters for COPY object from source to target API.**

NOTE: `copies from private to public bucket by default`

API URL: `http://localhost:5000/api/copy`

METHOD: `PUT`

BODY:
```json
{
  "Bucket": "publicImage",
  "Target": "branded-cars/bmw-x1-top-view-117.jpg",
  "Source": "cars/bmw-x1-top-view-117.jpg"
}
```

**Parameters for MOVE object from source to target API.**

NOTE: `copies from private to public bucket by default`

API URL: `http://localhost:5000/api/move`

METHOD: `PUT`

BODY:
```json
{
  "Bucket": "publicImage",
  "Target": "branded-cars/bmw-x1-top-view-117.jpg",
  "Source": "cars/bmw-x1-top-view-117.jpg"
}
```

**Parameters for DOWNLOAD object API.**

NOTE: `download from private/public bucket`

API URL: `http://localhost:5000/api/download`

METHOD: `POST`

BODY:
```json
{
  "Key": "bmw-x4.jpg",
  "Bucket": "privateImage"
}
```

**Parameters for UPLOAD object API.**

NOTE: `upload into public bucket`

API URL for CUSTOM UPLOAD: `http://localhost:5000/api/upload`

API URL for ALL FILES UPLOAD: `http://localhost:5000/api/upload-sharp`

METHOD: `POST`

FORMDATA:
```
media: File or files
```

**Parameters for DELETE objects API.**

API URL: `http://localhost:5000/api/delete`

METHOD: `DELETE`

BODY:
```json
{
  "Bucket": "publicImage",
  "Keys": ["media-wall4.jpg", "media-wall7.jpg", "media-wall6.png"]
}
```

Here it continues...