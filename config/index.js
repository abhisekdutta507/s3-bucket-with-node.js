const express = require('express')
const aws = require('aws-sdk')
const multer = require('multer')
const s3Storage = require('multer-sharp-s3')
const { s3 } = require('../environment/')

const File = require('../helper/file')
const Bucket = require('../api/Bucket/controller')

/**
 * @description 1 way upload using s3 sharp
 */
aws.config.update({ region: s3.region, accessKeyId: s3.accessKeyId, secretAccessKey: s3.secretAccessKey })
const bucket = new aws.S3({})
const USharp = multer({
  storage: s3Storage({
    s3: bucket,
    Bucket: s3.bucket.publicImage,
    ACL: 'public-read',
    Key: File.getKey
  })
})

/**
 * @description 2 way upload using multer & aws sdk
 */
const storage = multer.diskStorage({ destination: './upload/', filename: File.getKey })
const UMedia = multer({ storage })

const api = express.Router()

/**
 * @description version 1 API routes
 */
const routes = () => {
  api.get('/test', Bucket.find)
  api.put('/copy', Bucket.copy)
  api.put('/move', Bucket.move)
  api.put('/transfer', Bucket.transfer)
  api.post('/download', Bucket.download)
  api.post('/upload-sharp', USharp.array('media'), Bucket.uploadSharp)
  api.post('/upload', UMedia.array('media'), Bucket.upload)
  api.delete('/delete', Bucket.delete)

  return api
}

module.exports = routes